// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Curves/CurveVector.h"

#include "The_Arena/ActorComponents/TransformController.h"

#include "WeaponTransformController.generated.h"


/**
 * 
 */
UCLASS(Blueprintable, BlueprintType, meta = (BlueprintSpawnableComponent))
class THE_ARENA_API UWeaponTransformController : public UTransformController
{
	GENERATED_BODY()
	//Methods
public:
	void SetScaleOfMoving(float Scale) override;
protected:
	UWeaponTransformController();
	void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void RotateSceneComponent(float DeltaTime, FSceneCompRotParam& Param, float& PassAngle, FRotator direction) override;
	void MoveSceneComponent(float DeltaTime, FSceneCompMoveParam& Param, float& PassDistance, FVector direction) override;
	void SetLocationFromScale();
	void SetRotationFromScale();
	//Fields
protected:
	UPROPERTY(EditDefaultsOnly)
		 UCurveVector* RotationFromScaleCurve;
	UPROPERTY(EditDefaultsOnly)
		 UCurveVector* LocationFromScaleCurve;
	UPROPERTY(EditDefaultsOnly)
		float MultiplyScaleOfMovingParameter = 0.2f; // Needs for decrease values of scale of moving. For more currency.
};
