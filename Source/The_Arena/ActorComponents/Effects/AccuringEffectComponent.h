// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "The_Arena/ActorComponents/Effects/StandartEffectOnStat.h"
#include "AccuringEffectComponent.generated.h"


UCLASS(Blueprintable)
class THE_ARENA_API UAccuringEffectComponent : public UStandartEffectOnStat
{
	GENERATED_BODY()

	//Methods
public:
	void AccuringEffectInitialization( float InMaxAmountEffectPerSec, float InAmountEffectPerSec, TSubclassOf<UStatBaseComponent> InStatComponentClass, FCharacteristicEffectParams InEffectParams);
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetCurrentAmountEffectPerSec() const;

protected:
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	void CreateEffectInternal() override;
	void CreateStandartEffect() override;
	
	void CurrentAmountEffectPerSecInitialization();
	
	//Fields
protected:
	UPROPERTY()
	float MaxAmountEffectPerSec;

	UPROPERTY(Replicated)
	float CurrentAmountEffectPerSec;
};
