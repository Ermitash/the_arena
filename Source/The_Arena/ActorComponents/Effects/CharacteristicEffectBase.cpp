// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacteristicEffectBase.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

#include "The_Arena/UObjects/Widgets/EffectsInfoBaseWidget.h"


FName UCharacteristicEffectBase::GetEffectName() const
{
	return EffectParams.EffectName;
}

ECharacteristicEffectType UCharacteristicEffectBase::GetEffectType() const
{
	return EffectParams.EffectType;
}

float UCharacteristicEffectBase::GetActionTime() const
{
	return EffectParams.ActionTime;
}

void UCharacteristicEffectBase::CreateEffect()
{
	if (EffectParams.EffectType == ECharacteristicEffectType::NONE)
		return;

	if (EffectParams.EffectType == ECharacteristicEffectType::Endless)
	{
		GetWorld()->GetTimerManager().SetTimer(RemoveEffectTimerHandle, this, &UCharacteristicEffectBase::RemoveEffect, EffectParams.ActionTime, false);
	}
	
	CreateEffectInternal();

	bEffectCreated = true;
	OnRep_EffectCreated();
}

void UCharacteristicEffectBase::CreateEffectInternal()
{
	if (auto Owner = GetOwner())
	{
		TArray<UCharacteristicEffectBase*> CharacteristicEffects;
		Owner->GetComponents<UCharacteristicEffectBase>(CharacteristicEffects);

		for (auto CharacteristicEffect : CharacteristicEffects)
		{
			if (CharacteristicEffect)
			{
				if (CharacteristicEffect->GetClass() == GetClass() && CharacteristicEffect != this)
				{
					CharacteristicEffect->RemoveEffect();
				}
			}
		}
	}
	
	GetWorld()->GetTimerManager().SetTimer(RateTimeHandle, this, &UCharacteristicEffectBase::OnRate, Rate, true, 0);
}

void UCharacteristicEffectBase::OnRate()
{
	RateDispatcher.Broadcast();
}

void UCharacteristicEffectBase::RemoveEffect()
{
	DestroyComponent();
}

void UCharacteristicEffectBase::BeginPlay()
{
	Super::BeginPlay();

	if(bEffectCreated)
	{
		CreateEffectInfoWidget();
	}
}

void UCharacteristicEffectBase::CharacteristicEffectBaseInitialization(FCharacteristicEffectParams InEffectParams)
{
	EffectParams = InEffectParams;
}

FDMD& UCharacteristicEffectBase::GetRateDispatcher()
{
	return RateDispatcher;
}

void UCharacteristicEffectBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UCharacteristicEffectBase, EffectParams);
	DOREPLIFETIME(UCharacteristicEffectBase, bEffectCreated);
}

void UCharacteristicEffectBase::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	RemoveEffectInfoWidget();
	RemoveEffectInternal();
}

void UCharacteristicEffectBase::RemoveEffectInternal()
{
	GetWorld()->GetTimerManager().ClearTimer(RemoveEffectTimerHandle);
	GetWorld()->GetTimerManager().ClearTimer(RateTimeHandle);
}

void UCharacteristicEffectBase::CreateEffectInfoWidget()
{
	if(auto Pawn = Cast<APawn>(GetOwner()))
	{
		if(!Pawn->IsLocallyControlled())
		{
			return;
		}

		TArray<UObject*> Objects;
		GetObjectsOfClass(UEffectsInfoBaseWidget::StaticClass(), Objects);

		for(auto Object : Objects)
		{
			if(auto EffectsInfoWidget = Cast<UEffectsInfoBaseWidget>(Object))
			{
				if(Pawn->GetController() == EffectsInfoWidget->GetOwningPlayer() && EffectsInfoWidget->IsInViewport())
				{
					EffectsInfoWidget->CreateEffectInfoWidget(GetEffectName(), GetActionTime(), EffectParams.EffectType);
					break;
				}
			}
		}
	}
}

void UCharacteristicEffectBase::RemoveEffectInfoWidget()
{
	if (auto Pawn = Cast<APawn>(GetOwner()))
	{
		if (!Pawn->IsLocallyControlled())
		{
			return;
		}

		TArray<UObject*> Objects;
		GetObjectsOfClass(UEffectsInfoBaseWidget::StaticClass(), Objects);

		for (auto Object : Objects)
		{
			if (auto EffectsInfoWidget = Cast<UEffectsInfoBaseWidget>(Object))
			{
				if (Pawn->GetController() == EffectsInfoWidget->GetOwningPlayer() && EffectsInfoWidget->IsInViewport())
				{
					EffectsInfoWidget->RemoveEffectInfoWidget(GetEffectName());
					break;
				}
			}
		}
	}
}

void UCharacteristicEffectBase::OnRep_EffectCreated()
{
	CreateEffectInfoWidget();
}
