// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "The_Arena/ActorComponents/Effects/StopCharacteristicEffect.h"
#include "StopStatEffectBaseComponent.generated.h"

/**
 * 
 */
UCLASS()
class THE_ARENA_API UStopStatEffectBaseComponent : public UStopCharacteristicEffect
{
	GENERATED_BODY()
	
};
