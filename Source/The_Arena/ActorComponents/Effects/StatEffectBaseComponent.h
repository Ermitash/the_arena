// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "The_Arena/ActorComponents/Effects/CharacteristicEffectBase.h"
#include "The_Arena/ActorComponents/StatBaseComponent.h"

#include "StatEffectBaseComponent.generated.h"

/**
 * 
 */
UCLASS()
class THE_ARENA_API UStatEffectBaseComponent : public UCharacteristicEffectBase
{
	GENERATED_BODY()

	//Methods
public:
	void StatEffectBaseInitialization(TSubclassOf<UStatBaseComponent> InStatComponentClass, FCharacteristicEffectParams InEffectParams);
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
	UStatBaseComponent* GetStatComponent() const;
	
	//Fields
protected:
	UPROPERTY()
	TSubclassOf<UStatBaseComponent> StatComponentClass;
	UPROPERTY()
	UStatBaseComponent* StatComponent;
};
