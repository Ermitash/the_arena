// Fill out your copyright notice in the Description page of Project Settings.


#include "ModuleItemsManager.h"

#include "The_Arena/ActorComponents/WeaponModules/FatherConnectorComponent.h"

bool UModuleItemsManager::CanModuleAttachedToMotherConnector(AActor* Module, UMotherConnectorComponent* MotherConnector)
{
	if (!Module || !MotherConnector)
		return false;

	bool bAllowedAttachment = false;
	auto FatherConnector = Module->FindComponentByClass<UFatherConnectorComponent>();

	if (FatherConnector)
	{
		auto AcceptedFatherConnectorClasses = MotherConnector->GetAcceptedFatherConnectorClasses();
	
		for (auto FatherConnectorClass : AcceptedFatherConnectorClasses)
		{
			if (FatherConnector->GetClass() == FatherConnectorClass && !MotherConnector->GetAttachedFatherConnector())
			{
				bAllowedAttachment = true;
				break;
			}
		}
	}

	return bAllowedAttachment;
}

bool UModuleItemsManager::TryAttachModuleToMotherConnector(AActor* Module, UMotherConnectorComponent* MotherConnector)
{
	if (!Module || !MotherConnector || GetOwnerRole() != ROLE_Authority)
		return false;

	if(CanModuleAttachedToMotherConnector(Module, MotherConnector))
	{
		auto FatherConnector = Module->FindComponentByClass<UFatherConnectorComponent>();

		if (FatherConnector)
		{
			MotherConnector->SetupAttachment(FatherConnector);
			FatherConnector->SetupAttachment(MotherConnector);

			Module->AttachToComponent(MotherConnector, FAttachmentTransformRules(EAttachmentRule::KeepRelative, false));

			if(auto FatherParentComponent = FatherConnector->GetAttachParent())
			{
				auto ParentBoneTransform = FatherParentComponent->GetSocketTransform(FatherConnector->GetAttachSocketName(), ERelativeTransformSpace::RTS_Component);
				
				Module->SetActorRelativeLocation(FVector::ZeroVector - ParentBoneTransform.GetLocation());
				Module->SetActorRelativeRotation(FRotator::ZeroRotator - ParentBoneTransform.Rotator());
			}
			
			return true;
		}
	}

	return false;
}

bool UModuleItemsManager::TryDetachModuleFromMotherConnector(AActor* Module, UMotherConnectorComponent* MotherConnector)
{
	if (!Module || !MotherConnector || GetOwnerRole() != ROLE_Authority)
		return false;

	auto FatherConnector = Module->FindComponentByClass<UFatherConnectorComponent>();

	if (FatherConnector)
	{
		if(MotherConnector->GetAttachedFatherConnector() == FatherConnector)
		{
			Module->DetachFromActor(FDetachmentTransformRules(FAttachmentTransformRules::KeepWorldTransform, true));

			FatherConnector->RemoveAttachment();
			MotherConnector->RemoveAttachment();

			return true;
		}	
	}

	return false;
}

bool UModuleItemsManager::TryAttachModule(AActor* Module)
{
	if (!Module || GetOwnerRole() != ROLE_Authority)
		return false;

	for (auto MotherConnector : MotherConnectors)
	{
		if (MotherConnector)
		{
			if(TryAttachModuleToMotherConnector(Module, MotherConnector))
			{
				return true;
			}
		}
	}

	return false;
}

bool UModuleItemsManager::TryDetachModule(AActor* Module)
{
	if (!Module || GetOwnerRole() != ROLE_Authority)
		return false;

	for (auto MotherConnector : MotherConnectors)
	{
		if (MotherConnector && MotherConnector->GetAttachedFatherConnector())
		{
			if(TryDetachModuleFromMotherConnector(Module, MotherConnector))
			{
				return true;
			}
		}
	}

	return false;
}

bool UModuleItemsManager::CanModuleAttached(AActor* Module)
{
	if (!Module)
		return false;

	for (auto MotherConnector : MotherConnectors)
	{
		if(CanModuleAttachedToMotherConnector(Module, MotherConnector))
		{
			return true;
		}
	}

	return false;
}

TArray<UMotherConnectorComponent*> UModuleItemsManager::GetMotherConnectors() const
{
	return MotherConnectors;
}

TArray<AActor*> UModuleItemsManager::GetAttachedModuleItems() const
{
	TArray<AActor*> ModuleItems;

	for(auto MotherConnector : MotherConnectors)
	{
		if(auto AttachedModule = MotherConnector->GetAttachedModule())
		{
			ModuleItems.Add(AttachedModule);
		}	
	}

	return ModuleItems;
}

// Sets default values for this component's properties
UModuleItemsManager::UModuleItemsManager()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	SetIsReplicatedByDefault(true);
}


// Called when the game starts
void UModuleItemsManager::BeginPlay()
{
	Super::BeginPlay();

	Initialization();
}


// Called every frame
void UModuleItemsManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UModuleItemsManager::Initialization()
{	
	if(auto Owner = GetOwner())
	{
		Owner->GetComponents<UMotherConnectorComponent>(MotherConnectors);
	}
}

