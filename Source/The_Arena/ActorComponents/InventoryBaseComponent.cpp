// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryBaseComponent.h"

#include "The_Arena/Characters/FirstPersonCharacter.h"

// Sets default values for this component's properties
UInventoryBaseComponent::UInventoryBaseComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

void UInventoryBaseComponent::RemoveItemFromBaseInventory(AInventoryItemObject* InventoryItemObject)
{
	
}

void UInventoryBaseComponent::DragItem(AInventoryItemObject* InventoryItemObject,
	UDragAndDropManagerComponent* DragAndDropComponent)
{
	
}

void UInventoryBaseComponent::DropItem(UDragAndDropManagerComponent* DragAndDropComponent, bool bWithItemAmount)
{
	
}

void UInventoryBaseComponent::DropItemAt(int Tile, UDragAndDropManagerComponent* DragAndDropComponent, bool bWithItemAmount)
{
	
}

bool UInventoryBaseComponent::CanUsed(AActor* ActorUser)
{
	if(!ActorUser)
		return false;

	if (auto Owner = GetOwner())
	{
		auto HealthComponent = Owner->FindComponentByClass<UHealthComponent>();
		
		if (HealthComponent)
		{
			if (Owner == ActorUser)
			{
				if (!HealthComponent->IsAlive())
				{
					return false;
				}
			}
			else
			{
				if (HealthComponent->IsAlive())
				{
					return false;
				}
			}
		}

		float CurrentDistance = 0.0f;
		if (auto MeshComponent = Owner->FindComponentByClass<UMeshComponent>())
		{
			CurrentDistance = (MeshComponent->GetComponentLocation() - ActorUser->GetActorLocation()).Size();	
		}
		else
		{
			CurrentDistance = Owner->GetDistanceTo(ActorUser);
		}
		
		if (CurrentDistance > InteractionDistance)
		{
			return false;
		}

		return true;
	}

	return false;
}

// Called when the game starts
void UInventoryBaseComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UInventoryBaseComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

