// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneCaptureComponent2D.h"
#include "ScopeSceneCaptureComponent2D.generated.h"

/**
 * 
 */
UCLASS(meta = (BlueprintSpawnableComponent))
class THE_ARENA_API UScopeSceneCaptureComponent2D : public USceneCaptureComponent2D
{
	GENERATED_BODY()

	//Methods
public:
	virtual const AActor* GetViewOwner() const override;
	
};
