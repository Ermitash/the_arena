// Fill out your copyright notice in the Description page of Project Settings.


#include "TransformSequencer.h"
#include "The_Arena/Characters/FirstPersonCharacter.h"


void UTransformSequencer::OnBeginSequence_Implementation()
{
}

void UTransformSequencer::OnEndSequence_Implementation()
{
}

// Sets default values for this component's properties
UTransformSequencer::UTransformSequencer()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	TransformController = CreateDefaultSubobject<UTransformController>("TransformController");
}


// Called when the game starts
void UTransformSequencer::BeginPlay()
{
	Super::BeginPlay();

	// ...
	if (TransformController)
	{
		TransformController->SetScaleOfMoving(1.0f);
	}
}


// Called every frame
void UTransformSequencer::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (CanChangeLocation)
	{
		CheckLocationForUpdate();
		UpdateLocationParams(CurrentLocationIndex);
	}

	if (CanChangeRotation)
	{
		CheckRotationForUpdate();
		UpdateRotationParams(CurrentRotationIndex);
	}
}

void UTransformSequencer::UpdateLocationParams(int Index)
{
	if (TransformController)
	{
		if (LocationCoordinates.Num() > 0 && Index < LocationCoordinates.Num())
		{
			auto CurForwardSpeedLocation = CalculateForwardSpeedLocation(ForwardSpeedLocationCurve);

			TransformController->Forward.Location = LocationCoordinates[Index].Location.X;
			TransformController->Forward.ForwardSpeedLocation = CurForwardSpeedLocation.X * LocationCoordinates[Index].ForwardSpeed.X;
			TransformController->Right.Location = LocationCoordinates[Index].Location.Y;
			TransformController->Right.ForwardSpeedLocation = CurForwardSpeedLocation.Y * LocationCoordinates[Index].ForwardSpeed.Y;
			TransformController->Up.Location = LocationCoordinates[Index].Location.Z;
			TransformController->Up.ForwardSpeedLocation = CurForwardSpeedLocation.Z * LocationCoordinates[Index].ForwardSpeed.Z;
		}
	}
}

void UTransformSequencer::UpdateRotationParams(int Index)
{
	if (TransformController)
	{
		if (RotationCoordinates.Num() > 0 && Index < RotationCoordinates.Num())
		{
			auto CurForwardSpeedRotation = CalculateForwardSpeedRotation(ForwardSpeedRotationCurve);

			TransformController->Roll.Rotation = RotationCoordinates[Index].Rotation.Roll;
			TransformController->Roll.ForwardSpeedRotation = CurForwardSpeedRotation.X * RotationCoordinates[Index].ForwardSpeed.X;
			TransformController->Pitch.Rotation = RotationCoordinates[Index].Rotation.Pitch;
			TransformController->Pitch.ForwardSpeedRotation = CurForwardSpeedRotation.Y * RotationCoordinates[Index].ForwardSpeed.Y;
			TransformController->Yaw.Rotation = RotationCoordinates[Index].Rotation.Yaw;
			TransformController->Yaw.ForwardSpeedRotation = CurForwardSpeedRotation.Z * RotationCoordinates[Index].ForwardSpeed.Z;
		}
	}
}

FVector UTransformSequencer::CalculateForwardSpeedLocation(UCurveVector* CurForwardVectorCurve)
{
	if (!CurForwardVectorCurve)
		return FVector(1.0f, 1.0f, 1.0f);

	if (TransformController)
	{
		auto PassLocation = TransformController->GetPassLocation();

		auto CurSpeedX = CurForwardVectorCurve->GetVectorValue(PassLocation.X);
		auto CurSpeedY = CurForwardVectorCurve->GetVectorValue(PassLocation.Y);
		auto CurSpeedZ = CurForwardVectorCurve->GetVectorValue(PassLocation.Z);

		return FVector(CurSpeedX.X, CurSpeedY.Y, CurSpeedZ.Z);
	}

	return FVector(1.0f, 1.0f, 1.0f);
}

FVector UTransformSequencer::CalculateForwardSpeedRotation(UCurveVector* CurForwardVectorCurve)
{
	if (!CurForwardVectorCurve)
		return FVector(1.0f, 1.0f, 1.0f);

	if (TransformController)
	{
		auto PassRotation = TransformController->GetPassRotation();

		auto CurSpeedRoll = CurForwardVectorCurve->GetVectorValue(PassRotation.Roll);
		auto CurSpeedPitch = CurForwardVectorCurve->GetVectorValue(PassRotation.Pitch);
		auto CurSpeedYaw = CurForwardVectorCurve->GetVectorValue(PassRotation.Yaw);

		return FVector(CurSpeedRoll.X, CurSpeedPitch.Y, CurSpeedPitch.Z);
	}

	return FVector(1.0f, 1.0f, 1.0f);
}

void UTransformSequencer::CheckLocationForUpdate()
{
	if (TransformController)
	{
		if (CurrentLocationIndex >= 0 && CurrentLocationIndex < LocationCoordinates.Num() - 1)
		{
			if (TransformController->GetPassLocation().Equals(LocationCoordinates[CurrentLocationIndex].Location, CurrencyForUpdateLocationIndex))
			{
				SetCurrentLocationIndex(CurrentLocationIndex + 1);
			}
		}
	}
}

void UTransformSequencer::CheckRotationForUpdate()
{
	if (TransformController)
	{
		if (CurrentLocationIndex >= 0 && CurrentRotationIndex < RotationCoordinates.Num() - 1)
		{
			if (TransformController->GetPassRotation().Equals(RotationCoordinates[CurrentRotationIndex].Rotation, CurrencyForUpdateRotationIndex))
			{
				SetCurrentRotationIndex(CurrentRotationIndex + 1);
			}
		}
	}
}

int UTransformSequencer::GetCurrentLocationIndex() const
{
	return CurrentLocationIndex;
}

void UTransformSequencer::SetCurrentLocationIndex(int Index)
{
	if (Index >= 0 && Index < LocationCoordinates.Num())
	{
		CurrentLocationIndex = Index;
	}		
	else
	{
		CurrentLocationIndex = FMath::Clamp(Index, 0, LocationCoordinates.Num() - 1);
	}

	OnUpdateLocationIndexSequence(CurrentLocationIndex);
}

int UTransformSequencer::GetCurrentRotationIndex() const
{
	return CurrentRotationIndex;
}

void UTransformSequencer::SetCurrentRotationIndex(int Index)
{
	if (Index >= 0 && Index < RotationCoordinates.Num())
	{
		CurrentRotationIndex = Index;
	}		
	else
	{
		CurrentRotationIndex = FMath::Clamp(Index, 0, LocationCoordinates.Num() - 1);
	}
	
	OnUpdateRotationIndexSequence(CurrentRotationIndex);
}

const TArray<FSceneCompMoveSeqParam>* UTransformSequencer::GetLocationCoordinates() const
{
	return &LocationCoordinates;
}

const TArray<FSceneCompRotSeqParam>* UTransformSequencer::GetRotationCoordinates() const
{
	return &RotationCoordinates;
}

void UTransformSequencer::OnUpdateLocationIndexSequence_Implementation(int Index)
{
	UpdateLocationDelegate.Broadcast(Index);
}

void UTransformSequencer::OnUpdateRotationIndexSequence_Implementation(int Index)
{
	UpdateRotationDelegate.Broadcast(Index);
}

void UTransformSequencer::SetSceneComponent(USceneComponent* Component)
{
	TransformController->SetSceneComp(Component);
}

void UTransformSequencer::PlayLocationSequence()
{
	CanChangeLocation = true;
}

void UTransformSequencer::PlayLocationFragmentAt(int Index)
{
	UpdateLocationParams(Index);
}

void UTransformSequencer::PauseLocationSequence()
{
	CanChangeLocation = false;
}

void UTransformSequencer::StopLocationSequence()
{
	CanChangeLocation = false;
	ResetLocationSequence();
}

void UTransformSequencer::ResetLocationSequence()
{
	CurrentLocationIndex = 0;

	if (TransformController)
	{
		TransformController->Forward.Location = 0;
		TransformController->Forward.ForwardSpeedLocation = 0;
		TransformController->Right.Location = 0;
		TransformController->Right.ForwardSpeedLocation = 0;
		TransformController->Up.Location = 0;
		TransformController->Up.ForwardSpeedLocation = 0;
	}
}

void UTransformSequencer::PlayRotationSequence()
{
	CanChangeRotation = true;
}

void UTransformSequencer::PlayRotationFragmentAt(int Index)
{
	UpdateRotationParams(Index);
}

void UTransformSequencer::PauseRotationSequence()
{
	CanChangeRotation = false;
}

void UTransformSequencer::StopRotationSequence()
{
	CanChangeRotation = false;
	ResetRotationSequence();
}

void UTransformSequencer::ResetRotationSequence()
{
	CurrentRotationIndex = 0;

	if (TransformController)
	{
		TransformController->Roll.Rotation = 0;
		TransformController->Roll.ForwardSpeedRotation = 0;
		TransformController->Pitch.Rotation = 0;
		TransformController->Pitch.ForwardSpeedRotation = 0;
		TransformController->Yaw.Rotation = 0;
		TransformController->Yaw.ForwardSpeedRotation = 0;
	}
}
