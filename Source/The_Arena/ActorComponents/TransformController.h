// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SkeletalMeshComponent.h"
#include "Curves/CurveFloat.h"

#include "TransformController.generated.h"


USTRUCT(BlueprintType)
struct FSceneCompRotParam
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = RotProperties)
		float Rotation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = RotProperties, meta = (EditCondition = "!bUseDurationForForwardRotation"))
		float ForwardSpeedRotation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = RotProperties, meta = (EditCondition = "bUseDurationForForwardRotation"))
		float ForwardDuration;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = RotProperties)
		bool bUseDurationForForwardRotation = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = RotProperties, meta = (EditCondition = "!bUseDurationForBackRotation"))
		float BackSpeedRotation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = RotProperties, meta = (EditCondition = "bUseDurationForBackRotation"))
		float BackDuration;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = RotProperties)
		bool bUseDurationForBackRotation = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = RotProperties)
		bool Mirror = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MeshRotation)
		UCurveFloat* ForwardSpeedRotationCurve;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MeshRotation)
		UCurveFloat* BackSpeedRotationCurve;
};

USTRUCT(BlueprintType)
struct FSceneCompMoveParam
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PosProperties)
		float Location;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PosProperties, meta = (EditCondition = "!bUseDurationForForwardLocation"))
		float ForwardSpeedLocation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PosProperties, meta = (EditCondition = "bUseDurationForForwardLocation"))
		float ForwardDuration;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = PosProperties)
		bool bUseDurationForForwardLocation = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PosProperties, meta = (EditCondition = "!bUseDurationForBackLocation"))
		float BackSpeedLocation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PosProperties, meta = (EditCondition = "bUseDurationForBackLocation"))
		float BackDuration;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = PosProperties)
		bool bUseDurationForBackLocation = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = RotProperties)
		bool Mirror = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MeshLocation)
		UCurveFloat* ForwardSpeedLocationCurve;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MeshLocation)
		UCurveFloat* BackSpeedLocationCurve;
};

UCLASS(ClassGroup = (Custom), Blueprintable, BlueprintType, meta = (BlueprintSpawnableComponent))
class THE_ARENA_API UTransformController : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UTransformController();

	//Methods
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	void MoveParamInitialization(FSceneCompMoveParam& Param);
	void RotParamInitialization(FSceneCompRotParam& Param);
	void ParamsInitializtion();

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void RotateSceneComponent(float DeltaTime, FSceneCompRotParam& Param, float& PassAngle, FRotator direction);
	virtual void MoveSceneComponent(float DeltaTime, FSceneCompMoveParam& Param, float& PassDistance, FVector direction);

	UFUNCTION(BlueprintCallable)
		virtual void SetScaleOfMoving(float Scale);
	UFUNCTION(BlueprintCallable)
		float GetScaleOfMoving() const;

	virtual float GetSpeedCoef(float InTime, UCurveFloat* Curve) const;

	UFUNCTION(BlueprintCallable)
		void SetSceneComp(USceneComponent* Component);
	UFUNCTION(BlueprintCallable)
		USceneComponent* GetSceneComp() const;

	UFUNCTION(BlueprintCallable)
		FVector GetPassLocation() const;
	UFUNCTION(BlueprintCallable)
		void SetPassLocation(FVector Location);
	UFUNCTION(BlueprintCallable)
		FRotator GetPassRotation() const;
	UFUNCTION(BlueprintCallable)
		void SetPassRotation(FRotator Rotation);

	//Fields
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MeshRotation)
		FSceneCompRotParam Roll;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MeshRotation)
		FSceneCompRotParam Pitch;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MeshRotation)
		FSceneCompRotParam Yaw;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MeshLocation)
		FSceneCompMoveParam Forward;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MeshLocation)
		FSceneCompMoveParam Right;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MeshLocation)
		FSceneCompMoveParam Up;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float CurrencyScaleOfMoving = 0.001f;

	UPROPERTY()
	USceneComponent* SceneComp;

	float ScaleOfMoving;
	float PassRoll, PassYaw, PassPitch;
	float PassForward, PassRight, PassUp;
};
