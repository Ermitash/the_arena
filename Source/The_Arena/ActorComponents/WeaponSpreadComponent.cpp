// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponSpreadComponent.h"
#include "The_Arena/Actors/Weapons/RifleBase.h"


// Sets default values for this component's properties
UWeaponSpreadComponent::UWeaponSpreadComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UWeaponSpreadComponent::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void UWeaponSpreadComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	UpdateSpread();
}

FVector2D UWeaponSpreadComponent::GetRandomSpread()
{
	FVector2D ReturnSpread;

	ReturnSpread.X = FMath::FRandRange(-CurrentHorizonalSpread, CurrentHorizonalSpread);
	ReturnSpread.Y = FMath::FRandRange(-CurrentVerticalSpread, CurrentVerticalSpread);

	return ReturnSpread;
}

float UWeaponSpreadComponent::GetCurrentHorizonalSpread() const
{
	return CurrentHorizonalSpread;
}

float UWeaponSpreadComponent::GetCurrentVerticalSpread() const
{
	return CurrentVerticalSpread;
}

void UWeaponSpreadComponent::UpdateSpread()
{	
	if (auto FiringWeapon = Cast<ARifleBase>(GetOwner()))
	{
		float HorizontalSpread = FiringWeapon->GetHorizontalSpread();
		float VerticalSpread = FiringWeapon->GetVerticalSpread();

		CurrentHorizonalSpread = GetSpreadCoef(HorizontalSpreadCurve, HorizontalSpread);
		CurrentVerticalSpread = GetSpreadCoef(VerticalSpreadCurve, VerticalSpread);

		if (FiringWeapon->IsAiming())
		{
			CurrentHorizonalSpread *= AimingHorizontalSpreadMultiplier;
			CurrentVerticalSpread *= AimingVerticalSpreadMultiplier;
		}

		if (auto FPCharacter = Cast<AFirstPersonCharacter>(FiringWeapon->GetOwner()))
		{
			if(FPCharacter->bIsCrouched)
			{
				CurrentHorizonalSpread += CrouchingSpreadOffset;				
				CurrentVerticalSpread += CrouchingSpreadOffset;
			}
			
			auto MovementPriority = FPCharacter->GetMinMovementPriority();

			if((MovementPriority == EMovementType::FastWalk || MovementPriority == EMovementType::CrouchWalk) &&
				FPCharacter->GetCurrentVelocity() > BlockingSpreadSpeed &&
				(FPCharacter->GetVerticalMovementCondition() == EMovementCondition::Positive ||
					FPCharacter->GetVerticalMovementCondition() == EMovementCondition::Negative || 
					FPCharacter->GetHorizontalMovementCondition() == EMovementCondition::Positive ||
					FPCharacter->GetHorizontalMovementCondition() == EMovementCondition::Negative))
			{
				CurrentHorizonalSpread += WalkingSpreadOffset;
				CurrentVerticalSpread += WalkingSpreadOffset;
			}

			if(auto MovementComponent = FPCharacter->GetMovementComponent())
			{
				if(MovementComponent->IsFalling())
				{
					CurrentHorizonalSpread += JumpingSpreadOffset;
					CurrentVerticalSpread += JumpingSpreadOffset;
				}
			}

			if (CurrentHorizonalSpread < 0)
			{
				CurrentHorizonalSpread = 0;
			}

			if (CurrentVerticalSpread < 0)
			{
				CurrentVerticalSpread = 0;
			}
		}
	}
}

float UWeaponSpreadComponent::GetSpreadCoef(UCurveFloat* InCurve, float InSpread) const
{
	if(InCurve)
	{
		return InCurve->GetFloatValue(InSpread);
	}

	return 0.0f;
}

