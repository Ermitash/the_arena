// Fill out your copyright notice in the Description page of Project Settings.


#include "FPPlayerControllerRepParams.h"
#include "The_Arena/Characters/FirstPersonCharacter.h"

// Sets default values for this component's properties
UFPPlayerControllerRepParams::UFPPlayerControllerRepParams()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
	SetIsReplicatedByDefault(true);
}

void UFPPlayerControllerRepParams::BeginPlay()
{
	Super::BeginPlay();
	
	Initialization();
}

void UFPPlayerControllerRepParams::Initialization()
{	
	auto FPCharacter = Cast<AFirstPersonCharacter>(GetOwner());
	
	if (FPCharacter && FPCharacter->GetLocalRole() == ROLE_Authority)
	{	
		if (auto FPPlayerController = FPCharacter->GetController<AFirstPersonPlayerController>())
		{
			if(!bInitialized)
			{
				FPPlayerController->GetHoldingToCrouchDispatcher().AddDynamic(this, &UFPPlayerControllerRepParams::SetHoldingToCrouch);
				FPPlayerController->GetHoldingToAimDispatcher().AddDynamic(this, &UFPPlayerControllerRepParams::SetHoldingToAim);
				FPPlayerController->GetHoldingToSlowWalkDispatcher().AddDynamic(this, &UFPPlayerControllerRepParams::SetHoldingToSlowWalk);
				FPPlayerController->GetHoldingToRunDispatcher().AddDynamic(this, &UFPPlayerControllerRepParams::SetHoldingToRun);

				bInitialized = true;
			}			
		}
		else
		{	
			FPCharacter->GetControllerDispatcher().AddDynamic(this, &UFPPlayerControllerRepParams::Initialization);
		}
	}
}

void UFPPlayerControllerRepParams::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UFPPlayerControllerRepParams::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UFPPlayerControllerRepParams, bHoldingToCrouch);
	DOREPLIFETIME(UFPPlayerControllerRepParams, bHoldingToAim);
	DOREPLIFETIME(UFPPlayerControllerRepParams, bHoldingToSlowWalk);
	DOREPLIFETIME(UFPPlayerControllerRepParams, bHoldingToRun);
	DOREPLIFETIME(UFPPlayerControllerRepParams, bInitialized);
}

void UFPPlayerControllerRepParams::SetHoldingToCrouch(bool bInHoldingToCrouch)
{
	bHoldingToCrouch = bInHoldingToCrouch;
}

void UFPPlayerControllerRepParams::SetHoldingToAim(bool bInHoldingToAim)
{
	bHoldingToAim = bInHoldingToAim;
}

void UFPPlayerControllerRepParams::SetHoldingToSlowWalk(bool bInHoldingToSlowWalk)
{
	bHoldingToSlowWalk = bInHoldingToSlowWalk;
}

void UFPPlayerControllerRepParams::SetHoldingToRun(bool bInHoldingToRun)
{
	bHoldingToRun = bInHoldingToRun;
}

bool UFPPlayerControllerRepParams::IsHoldingToCrouch() const
{
	return bHoldingToCrouch;
}

bool UFPPlayerControllerRepParams::IsHoldingToAim() const
{
	return bHoldingToAim;
}

bool UFPPlayerControllerRepParams::IsHoldingToSlowWalk() const
{
	return bHoldingToSlowWalk;
}

bool UFPPlayerControllerRepParams::IsHoldingToRun() const
{
	return bHoldingToRun;
}