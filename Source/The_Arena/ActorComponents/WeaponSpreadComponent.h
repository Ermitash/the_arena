// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "WeaponSpreadComponent.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class THE_ARENA_API UWeaponSpreadComponent : public UActorComponent
{
	GENERATED_BODY()

	//Methods
public:
	UFUNCTION(BlueprintCallable)
	FVector2D GetRandomSpread();

	UFUNCTION(BlueprintCallable)
	float GetCurrentHorizonalSpread() const;
	UFUNCTION(BlueprintCallable)
	float GetCurrentVerticalSpread() const;
	
protected:
	UWeaponSpreadComponent();
	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	float GetSpreadCoef(UCurveFloat* InCurve, float InSpread) const;
	void UpdateSpread();
	
	//Fields
protected:
	UPROPERTY(EditDefaultsOnly)
		UCurveFloat* HorizontalSpreadCurve;
	UPROPERTY(EditDefaultsOnly)
		UCurveFloat* VerticalSpreadCurve;
	UPROPERTY(EditDefaultsOnly)
		float AimingHorizontalSpreadMultiplier;
	UPROPERTY(EditDefaultsOnly)
		float AimingVerticalSpreadMultiplier;

	UPROPERTY(EditDefaultsOnly)
		float CrouchingSpreadOffset = -0.2f;
	UPROPERTY(EditDefaultsOnly)
		float WalkingSpreadOffset = 0.2f;
	UPROPERTY(EditDefaultsOnly)
		float JumpingSpreadOffset = 0.2f;
	UPROPERTY(EditDefaultsOnly)
		float BlockingSpreadSpeed = 200.0f;

	float CurrentHorizonalSpread;
	float CurrentVerticalSpread;
};