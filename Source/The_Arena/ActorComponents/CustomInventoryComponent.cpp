// Fill out your copyright notice in the Description page of Project Settings.


#include "CustomInventoryComponent.h"

#include "The_Arena/Actors/MoneyItemObject.h"
#include "The_Arena/ActorComponents/TraderInventoryComponent.h"

void UCustomInventoryComponent::DropItem(UDragAndDropManagerComponent* DragAndDropComponent, bool bWithItemAmount)
{
	if (!DragAndDropComponent)
		return;

	auto DraggedItem = DragAndDropComponent->GetDraggedItem();

	if (Cast<AMoneyItemObject>(DraggedItem.InventoryItemObject))
	{
		return;
	}

	if(Cast<UTraderInventoryComponent>(DraggedItem.InventoryBaseComponent) ||
		Cast<UTraderBasketComponent>(DraggedItem.InventoryBaseComponent))
	{
		return;
	}
	
	Super::DropItem(DragAndDropComponent, bWithItemAmount);
}

void UCustomInventoryComponent::DropItemAt(int TopLeftIndex, UDragAndDropManagerComponent* DragAndDropComponent,
	bool bWithItemAmount)
{
	if (!DragAndDropComponent)
		return;

	auto DraggedItem = DragAndDropComponent->GetDraggedItem();

	if (Cast<AMoneyItemObject>(DraggedItem.InventoryItemObject))
	{
		return;
	}

	if (Cast<UTraderInventoryComponent>(DraggedItem.InventoryBaseComponent) ||
		Cast<UTraderBasketComponent>(DraggedItem.InventoryBaseComponent))
	{
		return;
	}

	Super::DropItemAt(TopLeftIndex, DragAndDropComponent, bWithItemAmount);
}

bool UCustomInventoryComponent::TryAddItem(AInventoryItemObject* InventoryItemObject, bool bWithItemAmount)
{
	if (Cast<AMoneyItemObject>(InventoryItemObject))
	{
		return false;		
	}

	return Super::TryAddItem(InventoryItemObject, bWithItemAmount);
}

bool UCustomInventoryComponent::TryAddItemAt(AInventoryItemObject* InventoryItemObject, int TopLeftIndex, bool bWithItemAmount)
{
	if (Cast<AMoneyItemObject>(InventoryItemObject))
	{
		return false;
	}
	
	return Super::TryAddItemAt(InventoryItemObject, TopLeftIndex, bWithItemAmount);
}

bool UCustomInventoryComponent::IsRoomAvailable(AInventoryItemObject* InventoryItemObject, int TopLeftIndex,
	bool bWithItemAmount)
{
	if (Cast<AMoneyItemObject>(InventoryItemObject))
	{
		return false;
	}
	
	return Super::IsRoomAvailable(InventoryItemObject, TopLeftIndex, bWithItemAmount);
}
