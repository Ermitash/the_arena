// Fill out your copyright notice in the Description page of Project Settings.


#include "FirstPersonPlayerController.h"
#include "The_Arena/GameMods/The_Arena_MatchMakingGameMode.h"
#include "The_Arena/ActorComponents/HealthComponent.h"

#include "GameFramework/PlayerStart.h"
#include "GameFramework/HUD.h"
#include "Blueprint/WidgetLayoutLibrary.h"
#include "GameFramework/SpectatorPawn.h"
#include "GameFramework/GameStateBase.h"

#include "Blueprint/UserWidget.h"
#include "EngineUtils.h"

DEFINE_LOG_CATEGORY(LogFirstPersonPlayerController);

void AFirstPersonPlayerController::ServerSelectTeam_Implementation(EGameTeam Team)
{	
	if (auto ArenaMatchMakingGameMode = Cast<AThe_Arena_MatchMakingGameMode>(GetWorld()->GetAuthGameMode()))
	{
		ArenaMatchMakingGameMode->SelectTeam(this, Team);
	}
}

void AFirstPersonPlayerController::PostSeamlessTravel()
{
	// Track the last completed seamless travel for the player
	LastCompletedSeamlessTravelCount = SeamlessTravelCount;
	
	CleanUpAudioComponents();

	if (PlayerCameraManager == nullptr)
	{
		SpawnPlayerCameraManager();
	}
}

void AFirstPersonPlayerController::NotifyLoadedWorld(FName WorldPackageName, bool bFinalDest)
{
	SetViewTarget(GetPawn());
	
	for (TActorIterator<APlayerStart> It(GetWorld()); It;)
	{
		APlayerStart* P = *It;

		FRotator SpawnRotation(ForceInit);
		SpawnRotation.Yaw = P->GetActorRotation().Yaw;
		SetInitialLocationAndRotation(P->GetActorLocation(), SpawnRotation);
		break;
	}
}

void AFirstPersonPlayerController::ClientRemoveAllWidgets_Implementation()
{
	UWidgetLayoutLibrary::RemoveAllWidgets(this);

	OnRemoveAllWidgets();
}

void AFirstPersonPlayerController::ClientSpectatorPawnLocationAndRotationInitialization_Implementation()
{
	for (TActorIterator<APlayerStart> It(GetWorld()); It; ++It)
	{
		APlayerStart* P = *It;

		if(P->PlayerStartTag == "Spectators")
		{
			FRotator SpawnRotation(ForceInit);
			SpawnRotation.Yaw = P->GetActorRotation().Yaw;
			SetInitialLocationAndRotation(P->GetActorLocation(), SpawnRotation);
			break;
		}		
	}
}

void AFirstPersonPlayerController::ClientSetSpectatorLocation_Implementation(FVector Location)
{
	if(auto SpectatorLocal = GetSpectatorPawn())
	{
		SpectatorLocal->SetActorLocation(Location);
	}
}

void AFirstPersonPlayerController::ClientSetSpectatorRotation_Implementation(FRotator Rotation)
{
	if (auto SpectatorLocal = GetSpectatorPawn())
	{
		SpectatorLocal->SetActorRotation(Rotation);
	}
}

void AFirstPersonPlayerController::StartDeathSpectating()
{
	if (GetLocalRole() == ROLE_Authority)
	{
		FTransform PawnTransform;

		if(auto PawnLocal = GetPawn())
		{
			PawnTransform = PawnLocal->GetTransform();
		}
		
		if (auto GameMode = GetWorld()->GetAuthGameMode())
		{
			ClientSetHUD(GameMode->HUDClass);
		}
		
		bDeathSpectatingActivated = true;
		ChangeState(NAME_Spectating);
		
		ClientStartDeathSpectating();
		ClientSetSpectatorLocation(PawnTransform.GetLocation());
		ClientSetSpectatorRotation(PawnTransform.Rotator());
	}
}

void AFirstPersonPlayerController::ClientStartDeathSpectating_Implementation()
{
	bDeathSpectatingActivated = true;
	ChangeState(NAME_Spectating);
}

void AFirstPersonPlayerController::SetPawn(APawn* InPawn)
{
	Super::Super::SetPawn(InPawn);

	if(InPawn)
	{
		bDeathSpectatingActivated = false;
	}
	
	// If we have a pawn we need to determine if we should show/hide the player for cinematic mode
	if (GetPawn() && bCinematicMode && bHidePawnInCinematicMode)
	{
		GetPawn()->SetActorHiddenInGame(true);
	}
}

void AFirstPersonPlayerController::StartSpectatingOnly()
{
	bDeathSpectatingActivated = false;
	ClientSetDeathSpectatingActivated(false);

	Super::StartSpectatingOnly();
}

void AFirstPersonPlayerController::ClientSetHUD_Implementation(TSubclassOf<AHUD> NewHUDClass)
{
	Super::ClientSetHUD_Implementation(NewHUDClass);
	HUDChangedDispatcher.Broadcast();
}

void AFirstPersonPlayerController::ChangeState(FName NewState)
{
	if(NewState != StateName)
	{
		Super::ChangeState(NewState);
	}
	else
	{
		if(StateName == NAME_Spectating)
		{
			auto WorldLocal = GetWorld();
			AGameStateBase* GameStateLocal = nullptr;
			auto SpectatorPawnLocal = GetSpectatorPawn();
			
			if(WorldLocal)
			{
				GameStateLocal = WorldLocal->GetGameState();
			}

			if(SpectatorPawnLocal && GameStateLocal)
			{
				if ((bDeathSpectatingActivated && SpectatorPawnLocal->GetClass() != DeathSpectatorPawnClass) || (!bDeathSpectatingActivated && SpectatorPawnLocal->GetClass() != GameStateLocal->SpectatorClass))
				{
					BeginSpectatingState();
					UpdateStateInputComponents();
				}
			}
		}
	}
}

void AFirstPersonPlayerController::PawnLeavingGame()
{
	if(bDestroyPawnAfterLeavingGame)
	{
		Super::PawnLeavingGame();
	}
	else
	{
		if(auto PawnLocal = GetPawn())
		{
			if(auto HealthComponent = PawnLocal->FindComponentByClass<UHealthComponent>())
			{
				HealthComponent->SetCurrentStat(0.0f);
			}
		}
	}
}

bool AFirstPersonPlayerController::IsHoldingToCrouch() const
{
	return bHoldingToCrouch;
}

void AFirstPersonPlayerController::SetHoldingToCrouch(bool bInHoldingToCrouch)
{
	if(GetLocalRole() == ROLE_AutonomousProxy)
	{
		ServerSetHoldingToCrouch(bInHoldingToCrouch);
	}

	bHoldingToCrouch = bInHoldingToCrouch;
	HoldingToCrouchDispatcher.Broadcast(bInHoldingToCrouch);
}

bool AFirstPersonPlayerController::IsHoldingToAim() const
{
	return bHoldingToAim;
}

void AFirstPersonPlayerController::SetHoldingToAim(bool bInHoldingToAim)
{
	if (GetLocalRole() == ROLE_AutonomousProxy)
	{
		ServerSetHoldingToAim(bInHoldingToAim);
	}

	bHoldingToAim = bInHoldingToAim;
	HoldingToAimDispatcher.Broadcast(bInHoldingToAim);
}

bool AFirstPersonPlayerController::IsHoldingToSlowWalk() const
{
	return bHoldingToSlowWalk;
}

void AFirstPersonPlayerController::SetHoldingToSlowWalk(bool bInHoldingToSlowWalk)
{
	if (GetLocalRole() == ROLE_AutonomousProxy)
	{
		ServerSetHoldingToSlowWalk(bInHoldingToSlowWalk);
	}

	bHoldingToSlowWalk = bInHoldingToSlowWalk;
	HoldingToSlowWalkDispatcher.Broadcast(bInHoldingToSlowWalk);
}

bool AFirstPersonPlayerController::IsHoldingToRun() const
{
	return bHoldingToRun;
}

void AFirstPersonPlayerController::SetHoldingToRun(bool bInHoldingToRun)
{
	if (GetLocalRole() == ROLE_AutonomousProxy)
	{
		ServerSetHoldingToRun(bInHoldingToRun);
	}

	bHoldingToRun = bInHoldingToRun;
	HoldingToRunDispatcher.Broadcast(bInHoldingToRun);
}

void AFirstPersonPlayerController::ServerSetHoldingToRun_Implementation(bool bInHoldingToRun)
{
	SetHoldingToRun(bInHoldingToRun);
}

void AFirstPersonPlayerController::ServerSetHoldingToSlowWalk_Implementation(bool bInHoldingToSlowWalk)
{
	SetHoldingToSlowWalk(bInHoldingToSlowWalk);
}

void AFirstPersonPlayerController::ServerSetHoldingToAim_Implementation(bool bInHoldingToAim)
{
	SetHoldingToAim(bInHoldingToAim);
}

FDMD_Bool& AFirstPersonPlayerController::GetHoldingToCrouchDispatcher()
{
	return HoldingToCrouchDispatcher;
}

FDMD_Bool& AFirstPersonPlayerController::GetHoldingToAimDispatcher()
{
	return HoldingToAimDispatcher;
}

FDMD_Bool& AFirstPersonPlayerController::GetHoldingToSlowWalkDispatcher()
{
	return HoldingToSlowWalkDispatcher;
}

FDMD_Bool& AFirstPersonPlayerController::GetHoldingToRunDispatcher()
{
	return HoldingToRunDispatcher;
}

FDMD& AFirstPersonPlayerController::GetRemoveAllWidgetsDispatcher()
{
	return RemoveAllWidgetsDispatcher;
}

FDMD& AFirstPersonPlayerController::GetHUDChangedDispatcher()
{
	return HUDChangedDispatcher;
}

bool AFirstPersonPlayerController::IsSelectTeamWidgetInitialized()
{
	return bSelectTeamWidgetInitialized;
}

void AFirstPersonPlayerController::SetSelectTeamWidgetInitialized(bool Value)
{
	bSelectTeamWidgetInitialized = Value;
}

void AFirstPersonPlayerController::MulticastSetSelectTeamWidgetInitialized_Implementation(bool Value)
{
	SetSelectTeamWidgetInitialized(Value);
}

void AFirstPersonPlayerController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AFirstPersonPlayerController, bHoldingToCrouch);
	DOREPLIFETIME(AFirstPersonPlayerController, bHoldingToAim);
	DOREPLIFETIME(AFirstPersonPlayerController, bHoldingToSlowWalk);
	DOREPLIFETIME(AFirstPersonPlayerController, bHoldingToRun);
}

void AFirstPersonPlayerController::BeginSpectatingState()
{
	if(bDeathSpectatingActivated)
	{
		if (GetPawn() != NULL && GetLocalRole() == ROLE_Authority && ShouldKeepCurrentPawnUponSpectating() == false)
		{
			UnPossess();
		}

		DestroySpectatorPawn();
		SetSpectatorPawn(SpawnDeathSpectatorPawn());
	}
	else
	{
		Super::BeginSpectatingState();
	}
}

ASpectatorPawn* AFirstPersonPlayerController::SpawnDeathSpectatorPawn()
{
	ASpectatorPawn* SpawnedSpectator = nullptr;

	// Only spawned for the local player
	if ((GetSpectatorPawn() == nullptr) && IsLocalController())
	{
		UWorld* World = GetWorld();
		
		FActorSpawnParameters SpawnParams;
		SpawnParams.Owner = this;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		SpawnParams.ObjectFlags |= RF_Transient;	// We never want to save spectator pawns into a map
		SpawnedSpectator = World->SpawnActor<ASpectatorPawn>(DeathSpectatorPawnClass, GetSpawnLocation(), GetControlRotation(), SpawnParams);
		if (SpawnedSpectator)
		{
			SpawnedSpectator->SetReplicates(false); // Client-side only
			SpawnedSpectator->PossessedBy(this);
			SpawnedSpectator->PawnClientRestart();
			if (SpawnedSpectator->PrimaryActorTick.bStartWithTickEnabled)
			{
				SpawnedSpectator->SetActorTickEnabled(true);
			}

			UE_LOG(LogFirstPersonPlayerController, Verbose, TEXT("Spawned spectator %s [server:%d]"), *GetNameSafe(SpawnedSpectator), GetNetMode() < NM_Client);
		}
		else
		{
			UE_LOG(LogFirstPersonPlayerController, Warning, TEXT("Failed to spawn spectator with class %s"), *GetNameSafe(DeathSpectatorPawnClass));
		}
	}

	return SpawnedSpectator;
}

void AFirstPersonPlayerController::ClientSetDeathSpectatingActivated_Implementation(bool bActivated)
{
	bDeathSpectatingActivated = bActivated;
}

void AFirstPersonPlayerController::OnRemoveAllWidgets_Implementation()
{
	RemoveAllWidgetsDispatcher.Broadcast();
}

void AFirstPersonPlayerController::ServerSetHoldingToCrouch_Implementation(bool bInHoldingToCrouch)
{
	SetHoldingToCrouch(bInHoldingToCrouch);
}
