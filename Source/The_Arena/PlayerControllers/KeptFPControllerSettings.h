// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "The_Arena/UObjects/ProjectTypes/CommonEnums.h"

#include "KeptFPControllerSettings.generated.h"

class AInventoryItemObject;

USTRUCT()
struct FKeptInventoryParams
{
	GENERATED_BODY()

	UPROPERTY()
	FName ItemObjectName;
	UPROPERTY()
	TArray<FName> ModuleItemObjectNames;
	
	int Amount;
};

USTRUCT()
struct FKeptEquipmentParams : public FKeptInventoryParams
{
	GENERATED_BODY()

	EEquipmentType EquipmentType;
};

UCLASS()
class THE_ARENA_API AKeptFPControllerSettings : public AActor
{
	GENERATED_BODY()

	//Methods
public:
	void SavePlayerData();
	void LoadPlayerData();

	void SetPlayerController(APlayerController* PlayerController);
	APlayerController* GetPlayerController() const;

	bool IsDataLoaded() const;
	
protected:
	AKeptFPControllerSettings();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	
	//Fields
protected:
	UPROPERTY()
	APlayerController* Controller;

	UPROPERTY()
	TArray<FKeptInventoryParams> KeptInventoryParams;
	UPROPERTY()
	TArray<FKeptEquipmentParams> KeptEquipmentParams;
	
	int Money;

	bool bDataLoaded = false;
	bool bDataSaved = false;

	bool bInventoryDataLoaded = false;
	bool bEquipmentDataLoaded = false;
	bool bMoneyDataLoaded = false;
};
