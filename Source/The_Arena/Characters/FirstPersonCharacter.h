// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "MatineeCameraShake.h"
#include "Components/SkeletalMeshComponent.h"
#include "Net/UnrealNetwork.h"

#include "The_Arena/ActorComponents/TransformController.h"
#include "The_Arena/ActorComponents/CharacterTransformController.h"
#include "The_Arena/PlayerControllers/FirstPersonPlayerController.h"
#include "The_Arena/ActorComponents/TransformSequencer.h"
#include "The_Arena/ActorComponents/WeaponTransformController.h"
#include "The_Arena/ActorComponents/FPSoundComponent.h"
#include "The_Arena/UObjects/DataAssets/PriorityWithOneEqualParamAsset.h"
#include "The_Arena/UObjects/ProjectTypes/CommonEnums.h"
#include "The_Arena/UObjects/DefaultDelegates.h"
#include "The_Arena/ActorComponents/FPPlayerControllerRepParams.h"
#include "The_Arena/ActorComponents/HealthComponent.h"
#include "The_Arena/ActorComponents/StaminaComponent.h"
#include "The_Arena/UObjects/DataAssets/JumpPriorityDataAsset.h"
#include "The_Arena/ActorComponents/InventoryComponent.h"
#include "The_Arena/ActorComponents/EquipmentComponent.h"
#include "The_Arena/Interfaces/Interactable.h"

#include "FirstPersonCharacter.generated.h"


USTRUCT(BlueprintType)
struct FMovementPriority
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	TEnumAsByte<EMovementType> Type;
	UPROPERTY()
	int Priority;
	UPROPERTY()
	FString Name;
};

UCLASS()
class THE_ARENA_API AFirstPersonCharacter : public ACharacter, public IInteractable
{
	GENERATED_BODY()
	
		////////////////////////////////////////Methods/////////////////////////////////////////////
protected:
	AFirstPersonCharacter();
	void BeginPlay() override;
	void OnRep_Controller() override;
	void Tick(float DeltaTime) override;
	void PossessedBy(AController* NewController) override;
	void UnPossessed() override;
	void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	
	float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	
	UFUNCTION()
	void ApplyDamageImpulse(FVector ImpulseDirection, FName BoneName, bool bVelChange);
	UFUNCTION(NetMulticast, Reliable)
	void MulticastApplyDamageImpulse(FVector ImpulseDirection, FName BoneName, bool bVelChange);
	
	UFUNCTION()
	bool CheckOnFriendlyFireInArenaGameMode(AActor* DamageCauser);
	void UpdateDeadBodyLocation();

	UFUNCTION()
	void OnHealthChanged();
	UFUNCTION()
	void OnDeath();
	
	UFUNCTION()
	void MoveVertical(float Value);
	UFUNCTION()
	void OnMoveVertical(float Value);
	UFUNCTION(Server, Reliable)
	void ServerOnMoveVertical(float Value);
	UFUNCTION(NetMulticast, Reliable)
	void MulticastOnMoveVertical(float Value);
	
	UFUNCTION()
	void MoveHorizontal(float Value);
	UFUNCTION()
	void OnMoveHorizontal(float Value);
	UFUNCTION(Server, Reliable)
	void ServerOnMoveHorizontal(float Value);
	UFUNCTION(NetMulticast, Reliable)
	void MulticastOnMoveHorizontal(float Value);

	void UpdateMovement();
	
	void LookUp(float Value);
	void TurnRight(float Value);

	UFUNCTION()
		void UpdateFPCameraPitchRotation();
	UFUNCTION(Server, Unreliable)
		void ServerUpdateFPCameraPitchRotation(float Pitch);

	//Doesn't replicate. Maybe fix it in future.
	void Incline(float Value);

	void Jump() override;
	void OnJumped_Implementation() override;
	UFUNCTION(NetMulticast, Reliable)
	void MulticastOnJumped_Implementation();  //While I turn off multicast, since it is not needed yet.
	
	void Landed(const FHitResult& Hit) override;
	UFUNCTION(NetMulticast, Reliable)
	void MulticastLanded(const FHitResult& Hit);  //While I turn off multicast, since it is not needed yet.
	
	void Falling() override;
	
	void CrouchCharacter(bool bButtonPressed);
	void CrouchWithHoldingButton(bool bButtonPressed);
	void CrouchWithoutHoldingButton(bool bButtonPressed);
	void OnStartCrouch(float HalfHeightAdjust, float ScaledHalfHeightAdjust) override;
	void OnEndCrouch(float HalfHeightAdjust, float ScaledHalfHeightAdjust) override;

	void SlowWalkAction(bool bButtonPressed);
	UFUNCTION(Server, Reliable)
	void ServerSlowWalkAction(bool bButtonPressed);
	UFUNCTION(NetMulticast, Reliable)
	void MulticastSlowWalkAction(bool bButtonPressed);
	void SlowWalkActionWithHoldingButton(bool bButtonPressed);
	void SlowWalkActionWithoutHoldingButton(bool bButtonPressed);
	void OnStartSlowWalk();
	void OnEndSlowWalk();
	
	void RunAction(bool bButtonPressed);
	UFUNCTION(Server, Reliable)
	void ServerRunAction(bool bButtonPressed);
	UFUNCTION(NetMulticast, Reliable)
	void MulticastRunAction(bool bButtonPressed);
	void RunActionWithHoldingButton(bool bButtonPressed);
	void RunActionWithoutHoldingButton(bool bButtonPressed);
	void OnStartRun();
	void OnEndRun();

	void DiagonalRunning();

	void CantWalk();
	void CrouchWalk();
	void SlowWalk();
	void FastWalk();
	void Run();

	void FireButtonPressed();
	void FireButtonReleased();
	void AimingButtonPressed();
	void AimingButtonReleased();
	void ReloadButtonPressed();
	void ChangeFireModeButtonPressed();

	UFUNCTION()
	void CheckOnInteraction();
	UFUNCTION()
	void InteractWithHittedActor(AActor* HittedActor);
	UFUNCTION(Server, Reliable)
	void ServerInteractWithHittedActor(AActor* HittedActor);
	UFUNCTION()
	void InteractionAction(bool bPressed);
	UFUNCTION()
	void OnStartHitInteraction();
	UFUNCTION()
	void OnUpdateHitInteraction();
	UFUNCTION()
	void OnEndHitInteraction();
	UFUNCTION()
	void OnInteraction(AActor* HittedActor);
	
	void SlotAction_0();
	void SlotAction_1();
	void SlotAction_2();
	void SlotAction_3();
	void SlotAction_4();
	void SlotAction_5();
	void SlotAction_6();
	void SlotAction_7();
	void SlotAction_8();
	void SlotAction_9();

	void UseEquipment(EEquipmentType EquipmentType);
	void ChangeActiveSlot(int Slot);
	void PlayCameraShake();

	UFUNCTION()
	void UpdateDefaultCharacterSkeletalMesh();

	//MeshTransformParams. Decoration. Only on clients.
	void PauseRotateInJump(int Index);
	void PauseMoveInJump(int Index);
	void PauseRotateInCrouch(int Index);
	void PauseMoveInCrouch(int Index);
	void PauseRotateInUncrouch(int Index);
	void PauseMoveInUncrouch(int Index);

	void MoveMeshHorizontally(float Value);
public:
	FString GetInteractionText_Implementation() override;
	bool CanInteracted_Implementation() const override;
	
	UFUNCTION()
	void AddMovementPriority(EMovementType Type, FString PriorityName);
	UFUNCTION(NetMulticast, Reliable)
	void MulticastAddMovementPriority(EMovementType Type, const FString& PriorityName);
	UFUNCTION()
	bool FindMovementPriority(EMovementType Type, FString PriorityName);
	UFUNCTION()
	void RemoveMovementPriority(EMovementType Type, FString PriorityName);
	UFUNCTION(NetMulticast, Reliable)
	void MulticastRemoveMovementPriority(EMovementType Type, const FString& PriorityName);
	UFUNCTION()
	EMovementType GetMinMovementPriority() const;
	
	UFUNCTION()
	void AddJumpPriority(FString PriorityName);
	UFUNCTION()
	void RemoveJumpPriority(FString PriorityName);
	UFUNCTION()
	EAbility GetMinJumpPriority() const;

	FDMD& GetJumpedDispatcher();
	FDMD& GetFallingDispatcher();
	FDMD& GetLandedDispatcher();

	FDMD& GetStartCrouchDispatcher();
	FDMD& GetEndCrouchDispatcher();

	FDMD& GetStartSlowWalkDispatcher();
	FDMD& GetEndSlowWalkDispatcher();

	FDMD& GetStartRunDispatcher();
	FDMD& GetEndRunDispatcher();
	
	FDMD_Float& GetMoveVerticalDispatcher();
	FDMD_Float& GetMoveHorizontalDispatcher();

	FDMD& GetControllerDispatcher();
	FDMD& GetDeathDispatcher();
	FDMD_C& Get_PreDeath_Controller_Dispatcher();
	FDMD_C& Get_Death_Controller_Dispatcher();

	FDMD_C& Get_UnPossess_Controller_Dispatcher();

	FDMD& GetStartHitInteractionDispatcher();
	FDMD& GetUpdateHitInteractionDispatcher();
	FDMD& GetEndHitInteractionDispatcher();

	FDMD_Actor& GetInteractionDispatcher();

	UFUNCTION(BlueprintCallable)
	UFPPlayerControllerRepParams* GetFPPlayerControllerRepParams() const;

	UFUNCTION(BlueprintCallable, Category = "FirstPersonCamera")
	UCameraComponent* GetFirstPersonCamera() const;
	UFUNCTION(BlueprintCallable, Category = "FirstPersonCamera")
	void SetFirstPersonCamera(UCameraComponent* val);
	UFUNCTION(BlueprintCallable, Category = "FirstPersonCamera")
	float GetFPCameraPitchRotation() const;

	UFUNCTION(BlueprintCallable, Category = "FPSkeletonMesh")
	USkeletalMeshComponent* GetFirstPersonSkeletalMesh() const;
	UFUNCTION(BlueprintCallable, Category = "FPSkeletonMesh")
	void SetFirstPersonSkeletalMesh(USkeletalMeshComponent* FPMesh);
	UFUNCTION(BlueprintCallable, BlueprintPure)
	UAudioComponent* GetCharacterVoiceComponent() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	UInventoryComponent* GetFPInventoryComponent() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	UEquipmentComponent* GetFPEquipmentComponent() const;
	UFUNCTION(BlueprintCallable)
	UCharacterTransformController* GetFPInclineTransformController() const { return FPIncline; }
	UFUNCTION(BlueprintCallable)
	UHealthComponent* GetFPHealthComponent() const { return FPHealthComponent; }
	UFUNCTION(BlueprintCallable)
	UStaminaComponent* GetFPStaminaComponent() const { return FPStaminaComponent; }
	
	UFUNCTION(BlueprintCallable, Category = "ThirdPersonView")
	TSubclassOf<UAnimInstance> GetThirdPersonAnimBP() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
		USkeletalMesh* GetDefaultCharacterSkeletalMesh() const;

	UFUNCTION()
		UPriorityWithOneEqualParamAsset* GetMovementPriorityAsset() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
		AActor* GetCurrentHittedInteractableActor() const;

	UFUNCTION(BlueprintCallable)
		bool IsCrouchWalking() const;
	UFUNCTION(BlueprintCallable)
		bool IsSlowWalking() const;
	UFUNCTION(BlueprintCallable)
		bool IsFastWalking() const;
	UFUNCTION(BlueprintCallable)
		bool IsRunning() const;
	
	UFUNCTION(BlueprintCallable)
		bool IsAbleToJump() const;

	UFUNCTION(BlueprintCallable)
		bool IsAlive() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
		float GetCurrentVelocity();
	UFUNCTION(BlueprintCallable)
		float GetWalkingSpeed() const;
	UFUNCTION(BlueprintCallable)
		float GetRunningSpeed() const;

	UFUNCTION(BlueprintCallable)
		EMovementCondition GetVerticalMovementCondition() const;
	UFUNCTION(BlueprintCallable)
		EMovementCondition GetHorizontalMovementCondition() const;

	UFUNCTION(BlueprintCallable)
		FName GetFirstFiringWeaponSocketName() const;
	UFUNCTION(BlueprintCallable)
		FName GetSecondFiringWeaponSocketName() const;
	
	////////////////////////////////////////Fields/////////////////////////////////////////////
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UEquipmentComponent* FPEquipmentComponent;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UTransformController* HorizontalMovingMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UWeaponTransformController* HorizontalRotationMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UWeaponTransformController* VerticalRotationMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UTransformController* CameraCrouchMoving;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UTransformController* FPMeshMovingWhenIncline;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UCharacterTransformController* FPIncline;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UTransformSequencer* FPJumpingSequencer;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UTransformSequencer* FPCrouchSequencer;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UTransformSequencer* FPUncrouchSequencer;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UFPSoundComponent* FPSoundComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UAudioComponent* CharacterVoiceComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UHealthComponent* FPHealthComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UStaminaComponent* FPStaminaComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInventoryComponent* FPInventoryComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UFPPlayerControllerRepParams* FPPlayerControllerRepParams;
	
	UPROPERTY(EditDefaultsOnly, Category = "CharacterMovementParameters")
		float RunningSpeed = 800;
	UPROPERTY(EditDefaultsOnly, Category = "CharacterMovementParameters")
		float SpeedOfFastWalk = 500;
	UPROPERTY(EditDefaultsOnly, Category = "CharacterMovementParameters")
		float SpeedOfSlowWalk = 250;
	UPROPERTY(EditDefaultsOnly, Category = "CharacterMovementParameters")
		float DelayBetweenTwoJumps = 0.0f;

	UPROPERTY(EditDefaultsOnly, Category = "DeathParameters")
		float ClientDeadBodyLocationUpdateFrequency = 0.0f;
	UPROPERTY(EditDefaultsOnly, Category = "DeathParameters")
		float ToleranceBetweenServerAndClientDeadBodyLocations = 0.0f;
	UPROPERTY(EditDefaultsOnly, Category = "DeathParameters")
		float UpdateDeadBodyLocationInterpSpeed = 0.0f;

	UPROPERTY(EditDefaultsOnly, Category = "ThirdPersonView")
		TSubclassOf<UAnimInstance> ThirdPersonAnimBP;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ThirdPersonView")
		USkeletalMesh* DefaultCharacterSkeletalMesh;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ThirdPersonView")
		USkeletalMesh* Team_1DefaultSkeletalMesh;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ThirdPersonView")
		USkeletalMesh* Team_2DefaultSkeletalMesh;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ThirdPersonView")
		FName FirstFiringWeaponSocketName;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ThirdPersonView")
		FName SecondFiringWeaponSocketName;
	
	UPROPERTY(EditDefaultsOnly, Category = "JumpingMeshParameters")
		int StartFlyLocationIndex = 0;
	UPROPERTY(EditDefaultsOnly, Category = "JumpingMeshParameters")
		int StartFlyRotationIndex = 0;
	UPROPERTY(EditDefaultsOnly, Category = "JumpingMeshParameters")
		int EndFlyLocationIndex = 0;
	UPROPERTY(EditDefaultsOnly, Category = "JumpingMeshParameters")
		int EndFlyRotationIndex = 0;

	UPROPERTY(EditDefaultsOnly, Category = "CrouchMeshParameters")
		int EndCrouchLocationIndex = 0;
	UPROPERTY(EditDefaultsOnly, Category = "CrouchMeshParameters")
		int EndCrouchRotationIndex = 0;
	UPROPERTY(EditDefaultsOnly, Category = "CrouchMeshParameters")
		int EndUncrouchLocationIndex = 0;
	UPROPERTY(EditDefaultsOnly, Category = "CrouchMeshParameters")
		int EndUncrouchRotationIndex = 0;

	UPROPERTY(EditDefaultsOnly, Category = "CameraShakeParameters")
		float CrouchBlockingCameraShakeSpeed = 0;
	UPROPERTY(EditDefaultsOnly, Category = "CameraShakeParameters")
		float SlowWalkBlockingCameraShakeSpeed = 0;
	UPROPERTY(EditDefaultsOnly, Category = "CameraShakeParameters")
		float FastWalkBlockingCameraShakeSpeed = 0;
	UPROPERTY(EditDefaultsOnly, Category = "CameraShakeParameters")
		float BlockingCameraShakeRunningSpeed = 0;
	UPROPERTY(EditDefaultsOnly, Category = "CameraShakeParameters")
		float ChanceToPlayTakeDamageCameraShake = 0;
	
	UPROPERTY(EditDefaultsOnly, Category = "CharacterShake")
		TSubclassOf<UMatineeCameraShake> FastWalkCameraShake;

	UPROPERTY(EditDefaultsOnly, Category = "CharacterShake")
		TSubclassOf<UMatineeCameraShake> RunCameraShake;

	UPROPERTY(EditDefaultsOnly, Category = "CharacterShake")
		TSubclassOf<UMatineeCameraShake> CrouchCameraShake;

	UPROPERTY(EditDefaultsOnly, Category = "CharacterShake")
		TSubclassOf<UMatineeCameraShake> SlowWalkCameraShake;

	UPROPERTY(EditDefaultsOnly, Category = "CharacterShake")
		TSubclassOf<UMatineeCameraShake> TakeDamageCameraShake;

	UPROPERTY(EditDefaultsOnly, Category = "Priority")
		UPriorityWithOneEqualParamAsset* MovementPriorityAsset;
	UPROPERTY(EditDefaultsOnly, Category = "Priority")
		UJumpPriorityDataAsset* JumpPriorityDataAsset;

	UPROPERTY(EditDefaultsOnly, Category = "Interaction")
		float DistanceToInteract = 300;
	
	UPROPERTY()
		UCameraComponent* FirstPersonCamera;
	UPROPERTY()
		USkeletalMeshComponent* FirstPersonSkeletonMesh;
	UPROPERTY()
		AActor* CurrentHittedInteractableActor;
	
	TSubclassOf<UMatineeCameraShake> CurrentCameraShake;

	UPROPERTY()
	TArray<FMovementPriority> MovementPriorities;
	UPROPERTY()
	TArray<FJumpPriorityParam> JumpPriorities;

	UPROPERTY(Replicated)
	float FPCameraPitchRotation;

	UPROPERTY(Replicated)
	FVector DeadBodyLocaiton;

	FTimerHandle UpdateDefaultCharacterSkeletalMeshHandle;
	FTimerHandle UpdateCameraPitchRotationHandle;
	FTimerHandle CameraShakeHandle;
	
	float deltaTime;
	float MoveHorizontalValue;
	float CurrentClientDeadBodyLocationUpdateTime = 0.0f;
	
	bool IsHorizontalMoving = false;
	bool bLanded = true;
	bool bIsIncline = false;
	bool bDoOnceCrouch = false;
	bool bDoOnceSlowWalk = false;
	bool bDoOnceRun = false;
	bool FastRightWalkPriorityAdded = false;
	bool FastBackWalkPriorityAdded = false;
	bool bSlowWalkPriorityAdded = false;
	bool bRunPriorityAdded = false;
	bool DiagonalRunningPriorityAdded = false;
	bool PlayedFlySequence = false;
	bool PlayedJumpSequence = false;
	bool bAllowedToInteract = false;
	bool bCanInteracted = false;

	UPROPERTY()
		USceneComponent* FPRootComponent;

	EMovementCondition VerticalMovementCondition = EMovementCondition::Zero;
	EMovementCondition HorizontalMovementCondition = EMovementCondition::Zero;
	
	//Events	
	UPROPERTY(BlueprintAssignable)
	FDMD JumpedDispatcher;
	UPROPERTY(BlueprintAssignable)
	FDMD FallingDispatcher;
	UPROPERTY(BlueprintAssignable)
	FDMD LandedDispatcher;

	UPROPERTY(BlueprintAssignable)
	FDMD StartCrouchDispatcher;
	UPROPERTY(BlueprintAssignable)
	FDMD EndCrouchDispatcher;

	UPROPERTY(BlueprintAssignable)
	FDMD StartSlowWalkDispatcher;
	UPROPERTY(BlueprintAssignable)
	FDMD EndSlowWalkDispatcher;
	
	UPROPERTY(BlueprintAssignable)
	FDMD StartRunDispatcher;
	UPROPERTY(BlueprintAssignable)
	FDMD EndRunDispatcher;
	
	UPROPERTY(BlueprintAssignable)
	FDMD_Float MoveVerticalDispatcher;
	UPROPERTY(BlueprintAssignable)
	FDMD_Float MoveHorizontalDispatcher;
	
	UPROPERTY(BlueprintAssignable)
	FDMD ControllerDispatcher;
	UPROPERTY(BlueprintAssignable)
	FDMD DeathDispatcher;
	UPROPERTY(BlueprintAssignable)
	FDMD_C PreDeath_Controller_Dispatcher;
	UPROPERTY(BlueprintAssignable)
	FDMD_C Death_Controller_Dispatcher;

	UPROPERTY(BlueprintAssignable)
	FDMD_C UnPossess_Controller_Dispatcher;

	UPROPERTY(BlueprintAssignable)
	FDMD StartHitInteractionDispatcher;
	UPROPERTY(BlueprintAssignable)
	FDMD UpdateHitInteractionDispatcher;
	UPROPERTY(BlueprintAssignable)
	FDMD EndHitInteractionDispatcher;

	UPROPERTY(BlueprintAssignable)
	FDMD_Actor InteractionDispatcher;
};