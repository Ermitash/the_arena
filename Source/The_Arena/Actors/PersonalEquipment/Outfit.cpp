// Fill out your copyright notice in the Description page of Project Settings.


#include "Outfit.h"
#include "The_Arena/Characters/FirstPersonCharacter.h"
#include "The_Arena/PlayerStates/The_Arena_PlayerState.h"

USkeletalMesh* AOutfit::GetOutfitSkeletalMesh() const
{
	return Team_1_OutfitSkeletalMesh;
}

float AOutfit::GetPhysicalProtection() const
{
	return PhysicalProtection;
}

void AOutfit::SetPhysicalProtection(float Value)
{
	PhysicalProtection = Value;
}

AOutfit::AOutfit()
{
	NetUpdateFrequency = 20.0f;
	bReplicates = true;
}

void AOutfit::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AOutfit, bOutfitInitialized);
}

void AOutfit::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	OutfitDeinitialization();
}

void AOutfit::OutfitInitialization()
{
	if (auto FPCharacter = Cast<AFirstPersonCharacter>(GetOwner()))
	{
		if(auto PlayerState = FPCharacter->GetPlayerState<AThe_Arena_PlayerState>())
		{
			auto CurrentTeam = PlayerState->GetCurrentTeam();
			USkeletalMesh* CurrentSkeletalMesh = nullptr;
			
			switch (CurrentTeam) {
			case EGameTeam::Team_1:
				CurrentSkeletalMesh = Team_1_OutfitSkeletalMesh;
				break;
			case EGameTeam::Team_2:
				CurrentSkeletalMesh = Team_2_OutfitSkeletalMesh;
				break;
			default:
				break;
			}

			if(CurrentTeam == EGameTeam::Team_1 || CurrentTeam == EGameTeam::Team_2)
			{
				if (auto Mesh = FPCharacter->GetMesh())
				{
					Mesh->SetSkeletalMesh(CurrentSkeletalMesh, false);
				}

				if (GetLocalRole() == ROLE_Authority)
				{
					bOutfitInitialized = true;
				}

				return;
			}
		}	
	}

	FTimerHandle TimerHandle;
	GetWorldTimerManager().SetTimer(TimerHandle, this, &AOutfit::OutfitInitialization, 0.1f, false);
}

void AOutfit::OutfitDeinitialization()
{
	if (auto FPCharacter = Cast<AFirstPersonCharacter>(GetOwner()))
	{
		if (auto Mesh = FPCharacter->GetMesh())
		{
			Mesh->SetSkeletalMesh(FPCharacter->GetDefaultCharacterSkeletalMesh());

			if (GetLocalRole() == ROLE_Authority)
			{
				bOutfitInitialized = false;
			}
		}
	}
}

void AOutfit::OnRep_OutfitInitialized()
{
	if(bOutfitInitialized)
	{
		OutfitInitialization();
	}
	else
	{
		OutfitDeinitialization();
	}
}
