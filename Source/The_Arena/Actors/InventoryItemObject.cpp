// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryItemObject.h"
#include "Net/UnrealNetwork.h"

#include "The_Arena/UObjects/DataAssets/InventoryItemCommonParams.h"
#include "The_Arena/ActorComponents/WeaponModules/ModuleItemObjectsManager.h"
#include "The_Arena/ActorComponents/WeaponModules/ModuleItemsManager.h"
#include "The_Arena/GameStates/CustomGameState.h"
#include "The_Arena/ActorComponents/WeaponModules/TPItemCaptureComponent.h"
#include "The_Arena/ActorComponents/WeaponModules/ModuleItemObjectParams.h"

// Sets default values
AInventoryItemObject::AInventoryItemObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	NetUpdateFrequency = 20.0f;
	bReplicates = true;
}

void AInventoryItemObject::Initialization(const UInventoryItemParam* InInventoryItemParam)
{
	if(InInventoryItemParam)
	{
		SetCurrentItemAmount(InInventoryItemParam->ItemAmount);

		if(auto ModuleItemObjectsManager = FindComponentByClass<UModuleItemObjectsManager>())
		{
			for(auto ModuleItemObject : InInventoryItemParam->ModuleItemObjects)
			{
				if(ModuleItemObjectsManager->TryAttachModule(ModuleItemObject))
				{
					ModuleItemObject->SetOwner(GetOwner());
				}
			}
		}
	}
}

void AInventoryItemObject::ServerRotate_Implementation()
{
	bRotated = !bRotated;	
	OnRep_Rotated();
}

FDMD& AInventoryItemObject::GetRotateDispatcher()
{
	return RotateDispatcher;
}

FDMD& AInventoryItemObject::GetCurrentItemAmountDispatcher()
{
	return CurrentItemAmountDispatcher;
}

FDMD& AInventoryItemObject::GetCurrentItemCostDispatcher()
{
	return CurrentItemCostDispatcher;
}

FDMD& AInventoryItemObject::GetCurrentItemWeightDispatcher()
{
	return CurrentItemWeightDispatcher;
}

// Called when the game starts or when spawned
void AInventoryItemObject::BeginPlay()
{
	Super::BeginPlay();
	
	if(bDynamicIcon)
	{
		DynamicIconsInitialization();
	}
	
	CalculateCurrentItemCost();
	CalculateCurrentItemWeight();

	if(auto ModuleItemObjectsManager = FindComponentByClass<UModuleItemObjectsManager>())
	{
		ModuleItemObjectsManager->GetModuleItemObjectsDispatcher().AddDynamic(this, &AInventoryItemObject::CalculateCurrentItemCost);
		ModuleItemObjectsManager->GetModuleItemObjectsDispatcher().AddDynamic(this, &AInventoryItemObject::CalculateCurrentItemWeight);
	}
	
	CreateInventoryItemParam();
}

// Called every frame
void AInventoryItemObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AInventoryItemObject::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AInventoryItemObject, bRotated);
	DOREPLIFETIME(AInventoryItemObject, CurrentItemAmount);
	DOREPLIFETIME(AInventoryItemObject, CurrentItemCost);
	DOREPLIFETIME(AInventoryItemObject, CurrentItemWeight);
}

void AInventoryItemObject::SetCurrentItemAmount(int Amount)
{
	if(Amount > MaxItemAmount)
	{
		CurrentItemAmount = MaxItemAmount;
	}
	else
	{
		CurrentItemAmount = Amount;
	}

	if(GetLocalRole() == ROLE_Authority)
	{
		OnRep_CurrentItemAmount();
	}
}

void AInventoryItemObject::CalculateCurrentItemWeight()
{
	auto CurrentItemWeightLocal = ItemWeight * CurrentItemAmount;

	if (auto ModuleItemObjectsManager = FindComponentByClass<UModuleItemObjectsManager>())
	{
		auto ModuleItemObjects = ModuleItemObjectsManager->GetModuleItemObjects();

		for (int i = 0; i < ModuleItemObjects.Num(); ++i)
		{
			if (auto ModuleItemObject = ModuleItemObjects[i])
			{
				CurrentItemWeightLocal += ModuleItemObject->GetCurrentItemWeight();
			}
		}
	}

	CurrentItemWeight = CurrentItemWeightLocal;

	if (GetLocalRole() == ROLE_Authority)
	{
		OnRep_CurrentItemWeight();
	}
}

void AInventoryItemObject::CalculateCurrentItemCost()
{
	auto CurrentItemCostLocal = ItemCost * CurrentItemAmount;

	if(auto ModuleItemObjectsManager = FindComponentByClass<UModuleItemObjectsManager>())
	{
		auto ModuleItemObjects = ModuleItemObjectsManager->GetModuleItemObjects();

		for(int i = 0; i < ModuleItemObjects.Num(); ++i)
		{
			if(auto ModuleItemObject = ModuleItemObjects[i])
			{
				CurrentItemCostLocal += ModuleItemObject->GetCurrentItemCost();
			}
		}
	}

	CurrentItemCost = CurrentItemCostLocal;

	if (GetLocalRole() == ROLE_Authority)
	{
		OnRep_CurrentItemCost();
	}
}

void AInventoryItemObject::OnRep_Rotated()
{
	RotateDispatcher.Broadcast();
}

void AInventoryItemObject::OnRep_CurrentItemAmount()
{
	CalculateCurrentItemWeight();
	CalculateCurrentItemCost();
	
	CurrentItemAmountDispatcher.Broadcast();
}

void AInventoryItemObject::OnRep_CurrentItemCost()
{
	CurrentItemCostDispatcher.Broadcast();
}

void AInventoryItemObject::OnRep_CurrentItemWeight()
{
	CurrentItemWeightDispatcher.Broadcast();
}

FIntPoint AInventoryItemObject::GetDimensions()
{
	FIntPoint AdditionalDimensions = {0, 0};
	if(auto ModuleItemObjectsManager = FindComponentByClass<UModuleItemObjectsManager>())
	{
		TArray<AInventoryItemObject*> ModuleItemObjects = ModuleItemObjectsManager->GetModuleItemObjects();

		for(auto ModuleItemObject : ModuleItemObjects)
		{
			if(ModuleItemObject)
			{
				if(auto ModuleItemObjectParams = ModuleItemObject->FindComponentByClass<UModuleItemObjectParams>())
				{
					AdditionalDimensions += ModuleItemObjectParams->GetAdditionalDimensions();
				}
			}
		}
	}

	if (!bRotated)
	{
		return Dimensions + AdditionalDimensions;
	}

	return FIntPoint{ Dimensions.Y + AdditionalDimensions.Y, Dimensions.X + AdditionalDimensions.X };
}

UMaterialInterface* AInventoryItemObject::GetIcon()
{
	if(bDynamicIcon)
	{
		return bRotated ? DynamicIconRotated : DynamicIcon;
	}
	
	return bRotated ? IconRotated : Icon;
}

bool AInventoryItemObject::IsRotated() const
{
	return bRotated;
}

UInventoryItemCommonParams* AInventoryItemObject::GetInventoryItemCommonParams() const
{
	return InventoryItemCommonParams;
}

float AInventoryItemObject::GetItemWeight() const
{
	return ItemWeight;
}

float AInventoryItemObject::GetCurrentItemWeight() const
{
	return CurrentItemWeight;
}

int AInventoryItemObject::GetMaxItemAmount() const
{
	return MaxItemAmount;
}

int AInventoryItemObject::GetCurrentItemAmount() const
{
	return CurrentItemAmount;
}

int AInventoryItemObject::GetItemCost() const
{
	return ItemCost;
}

int AInventoryItemObject::GetCurrentItemCost() const
{
	return CurrentItemCost;
}

FText AInventoryItemObject::GetItemDescription() const
{
	return ItemDescription;
}

void AInventoryItemObject::CreateInventoryItemParam()
{
	InventoryItemParam = NewObject<UInventoryItemParam>();
}

void AInventoryItemObject::DynamicIconsInitialization()
{
	ItemCanvasRenderTarget = UCanvasRenderTarget2D::CreateCanvasRenderTarget2D(GetWorld(), UCanvasRenderTarget2D::StaticClass());
	DynamicIcon = UMaterialInstanceDynamic::Create(Icon, this);
	DynamicIconRotated = UMaterialInstanceDynamic::Create(IconRotated, this);

	if (ItemCanvasRenderTarget)
	{
		if (DynamicIcon)
		{
			DynamicIcon->SetTextureParameterValue("Texture", ItemCanvasRenderTarget);
		}

		if (DynamicIconRotated)
		{
			DynamicIconRotated->SetTextureParameterValue("Texture", ItemCanvasRenderTarget);
		}
	}

	if(auto ModuleItemObjectsManager = FindComponentByClass<UModuleItemObjectsManager>())
	{
		ModuleItemObjectsManager->GetModuleItemObjectsDispatcher().AddDynamic(this, &AInventoryItemObject::UpdateDynamicIcon);
	}

	UpdateDynamicIcon();
}

void AInventoryItemObject::UpdateDynamicIcon()
{
	if (auto CustomGameState = Cast<ACustomGameState>(GetWorld()->GetGameState()))
	{
		if (auto InventoryItemsManager = CustomGameState->GetInventoryItemsManager())
		{
			if (InventoryItemCommonParams)
			{
				FActorSpawnParameters SpawnParameters;
				SpawnParameters.Owner = GetOwner();

				FInventoryItemParams ItemParams;
				ItemParams.ItemObject = this;
				ItemParams.bTestItem = true;

				if (auto ModulableItem = InventoryItemsManager->CreateInventoryItem(InventoryItemCommonParams->GetClassOfInventoryItem(), SpawnParameters, ItemParams))
				{
					if (auto TPItem = ModulableItem->GetTPInventoryItem())
					{
						if (auto TPItemCaptureComponent = TPItem->FindComponentByClass<UTPItemCaptureComponent>())
						{
							auto SceneForCreatingIcon = GetWorld()->SpawnActor<AActor>(TPItemCaptureComponent->GetItemCreatingIconSceneClass(), TPItem->GetActorLocation(), TPItem->GetActorRotation());

							if (SceneForCreatingIcon)
							{
								if (auto SceneCapture = TPItemCaptureComponent->GetItemSceneCapture())
								{
									CaptureDynamicIcon(SceneCapture, TPItem);
								}

								SceneForCreatingIcon->Destroy();
							}
						}

					}

					ModulableItem->Destroy();
				}
			}
		}
		else
		{
			CustomGameState->GetInitializationDispatcher().AddDynamic(this, &AInventoryItemObject::UpdateDynamicIcon);
		}
	}
}

void AInventoryItemObject::CaptureDynamicIcon(USceneCaptureComponent2D* SceneCapture, ATPInventoryItem* TPInventoryItem)
{
	if (!SceneCapture || !TPInventoryItem)
		return;

	if (ItemCanvasRenderTarget)
	{
		auto DimensionsLocal = GetDimensions();

		if(IsRotated())
		{
			ItemCanvasRenderTarget->SizeX = DimensionsLocal.Y * 100;
			ItemCanvasRenderTarget->SizeY = DimensionsLocal.X * 100;
		}
		else
		{
			ItemCanvasRenderTarget->SizeX = DimensionsLocal.X * 100;
			ItemCanvasRenderTarget->SizeY = DimensionsLocal.Y * 100;
		}
		
		ItemCanvasRenderTarget->UpdateResource();

		FVector AdditionalSceneCaptureLocation = FVector::ZeroVector;
		
		if(auto ModuleItemObjectsManager = FindComponentByClass<UModuleItemObjectsManager>())
		{
			auto ModuleItemObjects = ModuleItemObjectsManager->GetModuleItemObjects();

			for(auto ModuleItemObject : ModuleItemObjects)
			{
				if(ModuleItemObject)
				{
					if(auto ModuleItemObjectParams = ModuleItemObject->FindComponentByClass<UModuleItemObjectParams>())
					{
						AdditionalSceneCaptureLocation += ModuleItemObjectParams->GetAdditionalSceneCaptureLocation();
					}
				}
			}
		}
		
		TArray<AActor*> VisibleActors = TPInventoryItem->GetVisibleActors();		
		VisibleActors.Add(TPInventoryItem);

		SceneCapture->AddRelativeLocation(AdditionalSceneCaptureLocation);
		SceneCapture->ShowOnlyActors = VisibleActors;
		SceneCapture->TextureTarget = ItemCanvasRenderTarget;
		SceneCapture->CaptureScene();
	}
}

UInventoryItemParam* AInventoryItemObject::GetInventoryItemParam()
{
	if(InventoryItemParam)
	{
		InventoryItemParam->ItemAmount = CurrentItemAmount;

		if(auto ModuleItemObjectsManager = FindComponentByClass<UModuleItemObjectsManager>())
		{
			InventoryItemParam->ModuleItemObjects = ModuleItemObjectsManager->GetModuleItemObjects();
		}
	}
	
	return InventoryItemParam;
}

bool AInventoryItemObject::TryIncreaseItemAmount(AInventoryItemObject* InInventoryItemObject)
{
	if (!InInventoryItemObject || GetLocalRole() != ROLE_Authority)
		return false;

	if(GetClass() == InInventoryItemObject->GetClass())
	{
		int ItemAmountInAnotherItemObject = InInventoryItemObject->GetCurrentItemAmount();
		
		if(CurrentItemAmount + ItemAmountInAnotherItemObject <= MaxItemAmount)
		{
			SetCurrentItemAmount(CurrentItemAmount + ItemAmountInAnotherItemObject);
			InInventoryItemObject->Destroy();
			return true;
		}
	}

	return false;
}

bool AInventoryItemObject::TryRemoveItems(int Amount)
{
	if (GetLocalRole() != ROLE_Authority)
		return false;
	
	if (Amount <= 0)
		return false;
	
	if(CurrentItemAmount - Amount > 0)
	{
		SetCurrentItemAmount(CurrentItemAmount - Amount);
		return true;
	}

	return false;
}

