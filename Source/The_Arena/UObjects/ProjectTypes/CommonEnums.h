// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "CommonEnums.generated.h"


UENUM(BlueprintType)
enum class EEquipmentType : uint8
{
    NONE = 0,
    Outfit = 1,
    AdditionalEquipment = 2,
    FirstFiringWeapon = 3,
    SecondFiringWeapon = 4,
    Pistol = 5,
    Melee = 6
};

UENUM(BlueprintType)
enum class EGameTeam : uint8
{
    NONE             UMETA(DisplayName = "NONE"),
    Team_1           UMETA(DisplayName = "Team_1"),
    Team_2           UMETA(DisplayName = "Team_2"),
    Spectators        UMETA(DisplayName = "Spectators"),
};

UENUM(BlueprintType)
enum EMovementType
{
    NONE_MovementType, CantWalk, CrouchWalk, SlowWalk, FastWalk, Run,
};

UENUM(BlueprintType)
enum EMovementCondition
{
    Positive, Zero, Negative
};


////////////////////////////////////////////////////////////////
/** Utility class for enum types */
UCLASS()
class THE_ARENA_API UCommonEnums : public UObject
{
	GENERATED_BODY()
	
};

//This class just example for enum using
/*UENUM(BlueprintType)
enum class EWeapon: uint8
{
    MAINWEAPON             UMETA(DisplayName = "MainWeapon"),
        SECONDWEAPON           UMETA(DisplayName = "SecondWeapon"),
        MELEE                  UMETA(DisplayName = "MeleeWeapon"),
        GRENADE                UMETA(DisplayName = "Grenade")
};*/
/////////////////////////////////////////////////////////////////