// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"

#include "The_Arena/UObjects/ProjectTypes/CommonEnums.h"

#include "DefaultDelegates.generated.h"


/**
*  Don't care about this class. Needed only for delegates compilation.
 */
UCLASS(Abstract)
class THE_ARENA_API UDefaultDelegates : public UObject
{
	GENERATED_BODY()

};

DECLARE_DELEGATE(FD);
DECLARE_DELEGATE_OneParam(FD_Bool, bool);
DECLARE_DELEGATE_OneParam(FD_EquipmentType, EEquipmentType);
DECLARE_DELEGATE_TwoParams(FD_Bool_Bool, bool, bool);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDMD);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDMD_Bool, bool, InParam);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDMD_Float, float, InParam);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDMD_PlayerState, APlayerState*, InParam);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDMD_Actor, AActor*, InParam);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDMD_PC, APlayerController*, InParam);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDMD_C, AController*, InParam);











