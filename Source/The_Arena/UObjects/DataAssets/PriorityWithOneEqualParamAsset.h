// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"

#include "PriorityWithOneEqualParamAsset.generated.h"


USTRUCT(BlueprintType)
struct FPriorityParam
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly)
	FString Name;
	UPROPERTY(EditDefaultsOnly)
	int Priority;
};

UCLASS(BlueprintType)
class THE_ARENA_API UPriorityWithOneEqualParamAsset : public UDataAsset
{
	GENERATED_BODY()

	//Methods
public:
	UFUNCTION(BlueprintCallable)
	int GetPriority(FString Name) const;
	//Fields
protected:
	UPROPERTY(EditDefaultsOnly)
	TArray<FPriorityParam> Priorities;
};
