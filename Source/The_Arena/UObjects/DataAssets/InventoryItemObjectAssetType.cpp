// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryItemObjectAssetType.h"

UClass* UInventoryItemObjectAssetType::GetInventoryItemObjectClass(FName ItemObjectName)
{
	auto InventoryClassElementType = InventoryItemClasses.FindByPredicate([ItemObjectName](const FInventoryItemObjectAssetStruct& Struct)
		{
			return Struct.ItemObjectName == ItemObjectName;
		});

	UClass* InventoryClass = nullptr;
	
	if(InventoryClassElementType)
	{
		if(auto TSubclass = InventoryClassElementType->ItemObjectClass)
		{
			InventoryClass = TSubclass.Get();
		}	
	}

	return InventoryClass;
}
